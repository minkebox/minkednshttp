/*
 * Original code from https://github.com/commonshost/playdoh
 * DNS rewrite and AWS Lambda integration by me.
 */

const toBuffer = require('base64url').toBuffer;
const DnsPkt = require('dns-packet');
const Resolver = require('./Resolver');

const {
  BadRequest,
  MethodNotAllowed,
  PayloadTooLarge,
} = require('http-errors');

const DNSErrorMap = {
  EFORMERR: 1,
  ESERVFAIL: 2,
  ENOTFOUND: 3,
  ENOTIMP: 4,
  EREFUSED: 5,
};

const accept = 'Accept';
const contentType = 'content-type';
const dohMediaType = 'application/dns-message';
const dohMaximumMessageLength = 65535;


module.exports.playdoh =
function playdoh ({
  protocol = 'udp4',
  resolverAddress = '',
  timeout = 10000,
  path = '/'
} = {}) {
  if (resolverAddress === '' || resolverAddress === 'localhost') {
    resolverAddress = protocol === 'udp6' ? '::1' : '127.0.0.1';
  }
  Resolver.setTimeout(timeout);
  Resolver.setServers(typeof resolverAddress === 'string' ? [ resolverAddress ] : resolverAddress);

  return async function playdoh (event, response, next) {
    if (event.headers[accept] !== dohMediaType) {
      return next();
    }
    if (event.path !== path) {
      return next();
    }

    //console.log(JSON.stringify(event, null, 2));
    let dnsMessage = null;
    switch (event.httpMethod) {
      case 'GET':
      {
        const dns = event.queryStringParameters['dns'];
        if (!dns) {
          return next(new BadRequest());
        }
        try {
          dnsMessage = toBuffer(dns);
        } catch (error) {
          return next(new BadRequest());
        }
        if (dnsMessage.length > dohMaximumMessageLength) {
          return next(new PayloadTooLarge());
        }
        break;
      }
      case 'POST':
      {
        try {
          if (event.isBase64Encoded) {
            dnsMessage = Buffer.from(event.body, 'base64');
          }
          else {
            dnsMessage = Buffer.from(event.body);
          }
          if (dnsMessage.length > dohMaximumMessageLength) {
            return next(new PayloadTooLarge());
          }
        } catch (error) {
          return next(new PayloadTooLarge());
        }
        break;
      }
      default:
        return next(new MethodNotAllowed());
    }
    if (dnsMessage.length < 2) {
      return next(new BadRequest());
    }

    const dnsreq = DnsPkt.decode(dnsMessage);
    //console.log(JSON.stringify(dnsreq, null, 2));
    switch (dnsreq.opcode) {
      case 'QUERY':
      {
        const question = dnsreq.questions[0];
        if (!question) {
          return next(new BadRequest());
        }
        let dnsres = {
          type: 'response',
          id: dnsreq.id,
          flags: dnsreq.flags & 0xfff0,
          questions: [ question ]
        };
        try {
          switch (question.type) {
            case 'A':
            {
              await Resolver.find(dnsres, question.name, [ 'A' ], [ 'A', 'CNAME' ], [ 'AAAA' ]);
              break;
            }
            case 'AAAA':
            {
              await Resolver.find(dnsres, question.name, [ 'AAAA' ], [ 'AAAA', 'CNAME' ], [ 'A' ]);
              break;
            }
            case 'CNAME':
            {
              await Resolver.find(dnsres, question.name, [ 'CNAME' ], [ 'CNAME' ], [ 'A', 'AAAA' ]);
              break;
            }
            case 'MX':
            {
              await Resolver.find(dnsres, question.name, [ 'MX' ], [ 'MX' ], [ 'CNAME' ]);
              break;
            }
            case 'NS':
            {
              await Resolver.find(dnsres, question.name, [ 'NS' ], [ 'NS' ], []);
              break;
            }
            case 'PTR':
            {
              await Resolver.find(dnsres, question.name, [ 'PTR' ], [ 'PTR' ], []);
              break;
            }
            case 'SOA':
            {
              await Resolver.find(dnsres, question.name, [ 'SOA' ], [ 'SOA' ], []);
              break;
            }
            case 'SRV':
            {
              await Resolver.find(dnsres, question.name, [ 'SRV' ], [ 'SRV' ], []);
              break;
            }
            case 'TXT':
            {
              await Resolver.find(dnsres, question.name, [ 'TXT', 'CNAME', 'SOA' ], [ 'TXT', 'CNAME' ], []);
              break;
            }
            case 'ANY':
            case 'NAPTR':
            default:
              dnsres.flags |= 2; // SERVFAIL
              break;
          }
        }
        catch (err) {
          console.log(err);
          dnsres.flags |= DNSErrorMap[err.errno] || 2 /* SERVFAIL */;
        }

        //console.log(JSON.stringify(dnsres, null, 2));
        response.isBase64Encoded = true;
        response.headers[contentType] = dohMediaType;
        response.body = DnsPkt.encode(dnsres).toString('base64');
        break;
      }
      default:
        return next(new BadRequest());
    }
  };
};
