const Cache = {

  cache: {},
  
  get: async function(name) {
    return JSON.parse(Cache.cache[name] || null);
  },
  
  update: async function(name, info) {
    Cache.cache[name] = JSON.stringify(info);
  }
};

module.exports = Cache;
