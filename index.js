const Playdoh = require('./Playdoh2').playdoh;

const DOH = Playdoh({
  protocol: 'udp4',
  resolverAddress: [ '1.1.1.1', '1.0.0.1', '8.8.8.8', '8.8.4.4' ],
  timeout: 3000
});

exports.handler = async (event) => {
  const response = {
    headers: {},
    statusCode: 200,
    body: ''
  };
  try {
    await DOH(event, response, (err) => {
      if (err) {
        response.statusCode = err.statusCode;
      }
      else {
        response.errorCode = 404;
      }
    });
    return response;
  }
  catch (err) {
    console.log(err);
    return {
      statusCode: 500
    };
  }
};
