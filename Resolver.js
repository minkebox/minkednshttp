const { createSocket } = require('dgram')
const DnsPkt = require('dns-packet');
const Cache = require('./Cache');

const {
  InternalServerError,
  BadGateway,
  GatewayTimeout,
} = require('http-errors');

const resolverPort = 53;
const resolverProtocol = 'udp4';
let resolverAddresses = [ '127.0.0.1' ];
let timeout = 5000;

const sections = [ 'answers', 'authorities', 'additionals' ];


const Resolver = {

  setServers: function(servers) {
    resolverAddresses = servers;
  },

  setTimeout: function(time) {
    timeout = time;
  },

  /*
   * Find the 'queries' DNS information for the 'name' host.
   * Complete the 'result' dns object with the requested 'answer' and 'additionals'.
   */
  find: async function(result, name, queries, answers, additionals) {
    const info = await Resolver._lookup(name, queries);
    if (info) {
      result.answers = Resolver._merge(info, answers);
      result.authorities = info.records.SOA;
      result.additionals = Resolver._merge(info, additionals);
    }
    return result;
  },

  /*
   * Merge queries together.
   */
  _merge: function(info, queries) {
    let merged = [];
    for (let i = 0; i < queries.length; i++) {
      merged = merged.concat(info.records[queries[i]]);
    }
    return merged;
  },

  /*
   * Return, and possibly update, all the DNS information we have for a given 'name'.
   * Named 'queries' will be updated if necessary.
   */
  _lookup: async function(name, queries) {
    const now = Math.floor(Date.now() / 1000);

    // Fetch info from cache, or create new empty entry
    const info = (await Cache.get(name)) || {
      name: name,
      updated: now,
      records: {
        A: [],
        AAAA: [],
        CNAME: [],
        MX: [],
        NS: [],
        PTR: [],
        SOA: [],
        SRV: [],
        TXT: []
      }
    };

    // Age out old records which have exceeded their TTL
    if (now > info.updated) {
      const diff = now - info.updated;
      info.updated = now;
      for (let type in info.records) {
        const record = info.records[type];
        for (let i = 0; i < record.length; i++) {
          record[i].ttl -= diff;
          if (record[i].ttl <= 0) {
            record.splice(i, 1);
            i--;
          }
        }
      }
    }

    // Make sure we have the records we requested
    for (let i = 0; i < queries.length; i++) {
      const query = queries[i];
      const record = info.records[query];
      if (record.length === 0) {
        try {
          const response = await Resolver._query(name, query);
          sections.forEach(key => {
            const answers = response[key];
            for (let i = 0; i < answers.length; i++) {
              const answer = answers[i];
              const records = info.records[answer.type];
              if (records) {
                let j = records.length - 1;
                for (; j >= 0; j--) {
                  if (records[j].data === answer.data) {
                    records[j] = answer;
                    break;
                  }
                }
                if (j < 0) {
                  records.push(answer);
                }
              }
            }
          });
        }
        catch (err) {
          if (err.errno !== 'ENODATA') {
            throw err;
          }
        }
      }
    }

    // Update the cache
    //console.log(JSON.stringify(info, null, 2));
    Cache.update(name, info);

    return info;
  },

  _query: async function(name, query) {
    //console.log(name, query);
    return new Promise((resolve, reject) => {
      try {
        const resolverAddress = resolverAddresses[Math.floor(Math.random() * resolverAddresses.length)];
        //console.log(resolverAddress);
        const nonceDnsId = Math.floor(Math.random() * 65536);
        const socket = createSocket(resolverProtocol);
        socket.bind();
        socket.once('error', () => reject(new BadGateway()));
        socket.once('listening', () => {
          const timer = setTimeout(() => {
            socket.close();
            reject(new GatewayTimeout());
          }, timeout)
          socket.once('close', () => clearTimeout(timer));
          const message = DnsPkt.encode({
            id: nonceDnsId,
            type: 'query',
            flags: 256,
            questions: [
              { name: name, type: query, class: 'IN' }
            ]
          });
          socket.send(message, resolverPort, resolverAddress);
        })
        socket.on('message', (message, { port, address }) => {
          if (message.length < 2 || message.readUInt16BE(0) !== nonceDnsId || address !== resolverAddress || port !== resolverPort) {
            return reject(new BadGateway());
          }
          socket.close();
          //console.log(DnsPkt.decode(message));
          resolve(DnsPkt.decode(message));
        });
      }
      catch (error) {
        return reject(new InternalServerError());
      }
    });
  }
};

module.exports = Resolver;
